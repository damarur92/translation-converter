package es.damarur92.translationconverter.util;

import java.io.FileOutputStream;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.Map.Entry;

import org.apache.commons.lang3.StringUtils;
import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.xssf.usermodel.XSSFSheet;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;

import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;

public class TranslationConverterService {

	private static int rowCount = 0;

	private TranslationConverterService() {
		// Hide public constructor
	}

	public static void transformFiles(List<String> names, List<byte[]> files) throws Exception {
		System.out.println("--- Inicio Proceso ---");
		ObjectMapper mapper = new ObjectMapper();
		List<JsonNode> nodes = new ArrayList<>(files.size());
		for (byte[] file : files) {
			nodes.add(mapper.readValue(file, JsonNode.class));
		}
		System.out.println("--- Lectura JSON correcta ---");

		try (FileOutputStream outputStream = new FileOutputStream("src/main/resources/output/translations.xlsx");
				XSSFWorkbook workbook = new XSSFWorkbook();) {
			XSSFSheet sheet = workbook.createSheet("TRANSLATIONS");
			Row row = sheet.createRow(rowCount++);
			int columnCount = 0;
			Cell cell = row.createCell(columnCount++);
			cell.setCellValue("TAG");
			for (String name : names) {
				cell = row.createCell(columnCount++);
				cell.setCellValue(name);
			}
			for (JsonNode node : nodes) {
				printNodes(sheet, node, "", nodes.indexOf(node) + 1);
				System.out.println("--- Idioma " + names.get(nodes.indexOf(node)) + " tratado ---");
			}
			workbook.write(outputStream);
			System.out.println("--- Fin Proceso ---");
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	private static void printNodes(XSSFSheet sheet, JsonNode root, String tag, int columnCount) {
		Iterator<Entry<String, JsonNode>> iterador = root.fields();
		while (iterador.hasNext()) {
			StringBuilder stb = new StringBuilder(tag);
			Entry<String, JsonNode> entrada = iterador.next();
			if (stb.toString().length() > 0) {
				stb.append('.');
			}
			stb.append(entrada.getKey());
			if (entrada.getValue().isContainerNode()) {
				printNodes(sheet, entrada.getValue(), stb.toString(), columnCount);
			} else {
				// Checking if tag already exists
				int rowMatch = checkIfTagAlreadyExists(sheet, stb.toString());
				Row row;
				if (rowMatch > 0) {
					row = sheet.getRow(rowMatch);
				} else {
					row = sheet.getRow(rowCount);
					if (row == null) {
						row = sheet.createRow(rowCount);
					}
					rowCount++;
					System.out.println("--- Insertanto tag '" + stb.toString() + "' en fila " + rowCount + " ---");
					// TAG creation on first column 0-indexed
					Cell tagCell = row.createCell(0);
					tagCell.setCellValue(stb.toString());
				}
				System.out.println("--- Insertanto valor '" + entrada.getValue().asText() + "' en columna "
						+ columnCount + " ---");
				Cell valueCell = row.createCell(columnCount);
				valueCell.setCellValue(entrada.getValue().asText());
			}
		}
	}

	private static int checkIfTagAlreadyExists(XSSFSheet sheet, String tag) {
		for (int i = 1; i <= sheet.getLastRowNum(); i++) {
			Row row = sheet.getRow(i);
			if (row != null) {
				Cell cell = row.getCell(0);
				if (cell != null && StringUtils.equalsIgnoreCase(cell.getStringCellValue(), tag)) {
					System.out.println("--- Encontrado tag '" + tag + "' en la fila " + i + " ---");
					return i;
				}
			}
		}
		return 0;
	}
}
