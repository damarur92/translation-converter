package es.damarur92.translationconverter;

import java.io.InputStream;
import java.util.ArrayList;
import java.util.List;

import es.damarur92.translationconverter.util.TranslationConverterService;

/**
 * Example of the "main class". Put your bootstrap logic here.
 */
public class Main {

	public static void main(String[] args) throws Exception {
		InputStream caES = Main.class.getResourceAsStream("/input/ca_ES.json");
		InputStream esES = Main.class.getResourceAsStream("/input/es_ES.json");
		InputStream enGB = Main.class.getResourceAsStream("/input/en_GB.json");
		byte[] caESArray = new byte[caES.available()];
		caES.read(caESArray);
		byte[] esESArray = new byte[esES.available()];
		esES.read(esESArray);
		byte[] enGBArray = new byte[enGB.available()];
		enGB.read(enGBArray);
		List<String> names = new ArrayList<>(3);
		names.add("ca_ES");
		names.add("es_ES");
		names.add("en_GB");
		List<byte[]> files = new ArrayList<>(3);
		files.add(caESArray);
		files.add(esESArray);
		files.add(enGBArray);
		TranslationConverterService.transformFiles(names, files);
	}

}
